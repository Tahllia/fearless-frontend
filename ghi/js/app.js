function createCard(title, description, pictureUrl, starts, ends, location) { //dont forget to add new atributes inside the declatartion
    return `
        <div class="card"> //create css style here
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
            <h5 class="card-title">${title}</h5>
            <h7 class = "card-subtitle">${location}</h7>
            <p class="card-text">${description}</p>
        </div>
        </div>
        <div class = "card-footer">
        ${new Date(starts).toDateString()}
        ${new Date(ends).toDateString()}
        </div>
    `;
    }


    //remeber to look into this later
function createAlert(message) {
    var alertPlaceholder = document.getElementById('liveAlertPlaceholder')
    var wrapper = document.createElement('div')
    wrapper.innerHTML = `<div class="alert alert-primary" role="alert">
        The following error occurred: ${message}
        </div>`;
    alertPlaceholder.append(wrapper)
    
    }
    


window.addEventListener('DOMContentLoaded', async () => {

const url = 'http://localhost:8000/api/conferences/';

try {
    const response = await fetch(url);

    if (!response.ok) {
    // Figure out what to do when the response is bad
    } else {
    const data = await response.json();

    for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        
        if (detailResponse.ok) {
        const details = await detailResponse.json();
        console.log("details: ", details)
        const title = details.conference.name;
        const description = details.conference.description;
        const location = details.conference.location.name;
        const pictureUrl = details.conference.location.picture_url;
        const starts = details.conference.starts;
        const ends = details.conference.ends;
        const html = createCard(title, description, pictureUrl, starts, ends, location);
        console.log(title)


        const column = document.querySelector('.card-columns'); //Using DOM to call the querselector on the document and acess the class pased in 
        column.innerHTML += html;
        }
    }

    }
} catch (e) {
    // Figure out what to do if an error is raised
    createAlert(e);


}

});


// window.addEventListener('DOMContentLoaded', async () => {

// const url = 'http://localhost:8000/api/conferences/';

// try {
// const response = await fetch(url);

// if (!response.ok) {
// // Figure out what to do when the response is bad
//     }
//     else 
//     {
//     const data = await response.json();

//     for (let conference of data.conferences)
//     {
//     // const conference = data.conferences[0];
//     // const nameTag = document.querySelector('.card-title');
//     // nameTag.innerHTML = conference.name;

//     const detailUrl = `http://localhost:8000${conference.href}`; //not quotes-are backticks
//     const detailResponse = await fetch(detailUrl);

//     if (detailResponse.ok) 
//     {
        
//         // const description = document.querySelector(".card-text");
//         // const imageTag = document.querySelector(".card-img-top");

//         const details = await detailResponse.json();
//         const title = details.conference.title;
//         const description = details.conference.description;
//         const pictureUrl = details.conference.location.picture_url;
//         const html = createCard(title, description, pictureUrl);

//         const column = document.querySelector('.col');
//         column.innerHTML += html;
        
//         console.log(html);
//     }}}} 

//     catch (e) {
//     // Figure out what to do if an error is raised
// }

// });


